+++
title = "Portland Net Info"
description = ""
tags = [
    "reference",
    "nets",
    "portland",
]
date = "2019-08-26"
menu = "main"
+++

I've noticed a lot of net lists around the Portland (Oregon) area are 
incomplete or outdated. These are nets that I've confirmed are still active and
running. Please note, this is an early early work in progress, and is not 
yet complete.

The list was last updated as of the date listed above. All times listed below 
are local to Portland, Oregon.

Day | Time | Net Name | Frequency | Offset | Access
----|------|----------|-----------|--------|-------
Sunday | 19:00 | Portland NET Chat | 443.300 | + | 100.0
Sunday | 20:10 | Portland NET Net | 147.040 | + | 100.0
Sunday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Sunday | 18:05 | NTTN | 146.80 | - | 
Sunday | 23:00 | Insomniac Trivia | 440.400 | + | 
Monday | 18:05 | NTTN | 146.80 | - | 
Monday | 19:00 | Portland ARC | 146.80 | - | 
Monday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Monday | 20:00 | Sound Off Testing Net | 146.980 | - | DCS 023
Monday | 23:00 | Insomniac Trivia | 440.400 | + | 
Tuesday | 18:05 | NTTN | 146.80 | - | 
Tuesday | 19:00 | Swap Net | 146.980 | - | DCS 023
Tuesday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Tuesday | 23:00 | Insomniac Trivia | 440.400 | + | 
Wednesday | 18:05 | NTTN | 146.80 | - | 
Wednesday | 19:00 | Multnomah Co. ARES | 146.840 | - |
Wednesday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Wednesday | 20:05 | Skywarn Net | 147.32 | + | 100.0
Wednesday | 23:00 | Insomniac Trivia | 440.400 | + | 
Thursday | 18:05 | NTTN | 146.80 | - | 
Thursday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Thursday | 23:00 | Insomniac Trivia | 440.400 | + | 
Friday | 18:05 | NTTN | 146.80 | - | 
Friday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Friday | 19:30 | Amateur Radio Newsline | 146.98 | - | DCS 023
Friday | 20:00 | Anything Goes Open Net | 146.98 | - | DCS 023
Friday | 23:00 | Insomniac Trivia | 440.400 | + | 
Saturday | 18:05 | NTTN | 146.80 | - | 
Saturday | 19:30 | ARES District 1 | 147.320 | + | 100.0
Saturday | 23:00 | Insomniac Trivia | 440.400 | + | 
